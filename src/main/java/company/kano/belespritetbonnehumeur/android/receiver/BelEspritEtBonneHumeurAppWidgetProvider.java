package company.kano.belespritetbonnehumeur.android.receiver;

import company.kano.belespritetbonnehumeur.R;
import company.kano.belespritetbonnehumeur.android.bean.QuotationBean;
import company.kano.belespritetbonnehumeur.android.util.QuotationUtils;
import company.kano.belespritetbonnehumeur.android.util.Settings;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.util.TypedValue;
import android.widget.RemoteViews;
import androidx.preference.PreferenceManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BelEspritEtBonneHumeurAppWidgetProvider extends AppWidgetProvider {

	@Override
	public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, final int[] appWidgetIds) {
		log.debug("appWidgetIds: {}", (Object) appWidgetIds);

		super.onUpdate(context, appWidgetManager, appWidgetIds);

		int intentFlags = PendingIntent.FLAG_UPDATE_CURRENT;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			intentFlags |= PendingIntent.FLAG_IMMUTABLE;
		}

		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		int textPrimarySize = sharedPref.getInt(Settings.KEY_PREF_WIDGET_TEXT_SIZE, context.getResources().getInteger(R.integer.pref_text_size_default));
		int textPrimaryColor = sharedPref.getInt(Settings.KEY_PREF_WIDGET_TEXT_PRIMARY_COLOR, context.getResources().getColor(R.color.pref_text_primary_color_default));
		int textSecondaryColor = sharedPref.getInt(Settings.KEY_PREF_WIDGET_TEXT_SECONDARY_COLOR, context.getResources().getColor(R.color.pref_text_secondary_color_default));
		int backgroundColor = sharedPref.getInt(Settings.KEY_PREF_WIDGET_BACKGROUND_COLOR, context.getResources().getColor(R.color.pref_background_color_default));

		QuotationBean quotation = QuotationUtils.getQuotation(context);

		RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.belespritetbonnehumeur_appwidget);

		views.setTextViewText(R.id.quotation, quotation.getSpannedText(context, textPrimaryColor, textSecondaryColor, null));
		views.setTextViewTextSize(R.id.quotation, TypedValue.COMPLEX_UNIT_SP, textPrimarySize);
		if (quotation.getUrl() != null) {
			views.setOnClickPendingIntent(R.id.belespritetbonnehumeur_appwidget, PendingIntent.getActivity(context, quotation.getQuotation().hashCode(), new Intent(Intent.ACTION_VIEW, Uri.parse(quotation.getUrl())), intentFlags));
		} else {
			views.setOnClickPendingIntent(R.id.belespritetbonnehumeur_appwidget, null);
		}
		views.setInt(R.id.belespritetbonnehumeur_appwidget, "setBackgroundColor", backgroundColor);

		appWidgetManager.updateAppWidget(appWidgetIds, views);
	}

}
