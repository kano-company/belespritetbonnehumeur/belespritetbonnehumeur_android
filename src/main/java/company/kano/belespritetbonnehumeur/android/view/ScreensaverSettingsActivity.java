package company.kano.belespritetbonnehumeur.android.view;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

public class ScreensaverSettingsActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getSupportFragmentManager().beginTransaction().replace(android.R.id.content, new ScreensaverSettingsFragment()).commit();
	}

}
