package company.kano.belespritetbonnehumeur.android.view;

import company.kano.belespritetbonnehumeur.R;
import company.kano.belespritetbonnehumeur.android.receiver.BelEspritEtBonneHumeurAppWidgetProvider;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.preference.PreferenceFragmentCompat;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
		setPreferencesFromResource(R.xml.settings, rootKey);
	}

	@Override
	public void onResume() {
		super.onResume();

		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();

		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getContext());
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(getContext(), BelEspritEtBonneHumeurAppWidgetProvider.class));
		Intent update = new Intent();
		update.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
		update.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
		getContext().sendBroadcast(update);
	}

}
