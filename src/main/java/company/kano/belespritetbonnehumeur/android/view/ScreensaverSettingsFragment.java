package company.kano.belespritetbonnehumeur.android.view;

import company.kano.belespritetbonnehumeur.R;
import company.kano.belespritetbonnehumeur.android.preference.FontPreference;
import company.kano.belespritetbonnehumeur.android.util.Settings;

import android.os.Bundle;
import androidx.preference.PreferenceFragmentCompat;

public class ScreensaverSettingsFragment extends PreferenceFragmentCompat {

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
		setPreferencesFromResource(R.xml.screensaver_settings, rootKey);

		((FontPreference) findPreference(Settings.KEY_PREF_SCREENSAVER_TEXT_FONT)).registerForActivityResult(this);
	}

}
