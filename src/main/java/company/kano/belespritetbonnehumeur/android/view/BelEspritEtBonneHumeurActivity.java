package company.kano.belespritetbonnehumeur.android.view;

import company.kano.belespritetbonnehumeur.BuildConfig;
import company.kano.belespritetbonnehumeur.R;
import company.kano.belespritetbonnehumeur.android.bean.QuotationBean;
import company.kano.belespritetbonnehumeur.android.util.QuotationUtils;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class BelEspritEtBonneHumeurActivity extends AppCompatActivity {

	QuotationBean quotation;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.belespritetbonnehumeur);

		quotation = QuotationUtils.getQuotation(this);
		((TextView) findViewById(R.id.quotation)).setText(quotation.getSpannedText(this, null, null, null));
		if (quotation.getUrl() != null) {
			findViewById(R.id.belespritetbonnehumeur).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(quotation.getUrl())));
				}
			});
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_share:
				StringBuilder shareStringBuilder = new StringBuilder(getString(R.string.lbl_quotation, quotation.getQuotation()));
				if (quotation.getAuthor() != null) {
					shareStringBuilder.append('\n').append(quotation.getAuthor());
				}
				if (quotation.getWork() != null) {
					shareStringBuilder.append('\n').append(quotation.getWork());
				}
				shareStringBuilder.append('\n').append('\n').append(getString(R.string.app_name))
						.append('\n').append(getString(R.string.app_url))
						.append('\n').append(getString(R.string.playstore_url));
				Intent shareIntent = new Intent(Intent.ACTION_SEND);
				shareIntent.putExtra(Intent.EXTRA_TEXT, shareStringBuilder.toString());
				shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
				shareIntent.setType("text/plain");
				startActivity(shareIntent);
				return true;
			case R.id.action_settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			case R.id.action_about:
				View view = LayoutInflater.from(this).inflate(R.layout.about, null);
				((TextView) view.findViewById(R.id.lbl_about_version)).setText(getString(R.string.lbl_about_version, BuildConfig.VERSION_NAME));
				((TextView) view.findViewById(R.id.lbl_about_by)).setMovementMethod(LinkMovementMethod.getInstance()); // enable clicking on url
				((TextView) view.findViewById(R.id.lbl_about_site)).setMovementMethod(LinkMovementMethod.getInstance()); // enable clicking on url
				((TextView) view.findViewById(R.id.lbl_about_data_source)).setMovementMethod(LinkMovementMethod.getInstance()); // enable clicking on url
				((TextView) view.findViewById(R.id.lbl_about_code_source)).setMovementMethod(LinkMovementMethod.getInstance()); // enable clicking on url
				((TextView) view.findViewById(R.id.lbl_about_libraries)).setMovementMethod(LinkMovementMethod.getInstance()); // enable clicking on url
				new AlertDialog.Builder(this)
					.setTitle(R.string.title_about)
					.setView(view)
					.setIcon(R.mipmap.ic_launcher)
					.setPositiveButton(getString(R.string.ok), null)
					.show();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

}
