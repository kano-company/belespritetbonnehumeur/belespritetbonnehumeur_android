package company.kano.belespritetbonnehumeur.android.service;

import company.kano.belespritetbonnehumeur.R;
import company.kano.belespritetbonnehumeur.android.bean.QuotationBean;
import company.kano.belespritetbonnehumeur.android.util.FontUtils;
import company.kano.belespritetbonnehumeur.android.util.QuotationUtils;
import company.kano.belespritetbonnehumeur.android.util.Settings;

import java.util.Random;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.service.dreams.DreamService;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.preference.PreferenceManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
public class BelEspritEtBonneHumeurDreamService extends DreamService {

	private final Random random = new Random();
	private final Handler handler = new Handler(Looper.getMainLooper());
	private final Runnable updateRunner = new Runnable() {
		@Override
		public void run() {
			log.debug("updateRunner");
			View container = findViewById(R.id.quotation_container);

			Animator beginAnimator = ObjectAnimator.ofFloat(container, View.ALPHA, 1f, 0f).setDuration(2000L);
			beginAnimator.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					super.onAnimationEnd(animation);
					updateQuotation();
				}
			});

			Animator endAnimator = ObjectAnimator.ofFloat(container, View.ALPHA, 0f, 1f).setDuration(2000L);

			AnimatorSet allAnimator = new AnimatorSet();
			allAnimator.playSequentially(beginAnimator, endAnimator);
			allAnimator.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					super.onAnimationEnd(animation);
					scheduleUpdate();
				}
			});
			allAnimator.start();
		}
	};

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();

		setInteractive(false);
		setFullscreen(true);
		setContentView(R.layout.belespritetbonnehumeur_screensaver);

		DisplayMetrics metrics = new DisplayMetrics();
		((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
		int screenWidth = Math.min(metrics.widthPixels, metrics.heightPixels);

		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		int width = sharedPref.getInt(Settings.KEY_PREF_SCREENSAVER_TEXT_WIDTH, this.getResources().getInteger(R.integer.pref_screensaver_text_width_default));
		int textPrimarySize = sharedPref.getInt(Settings.KEY_PREF_SCREENSAVER_TEXT_SIZE, this.getResources().getInteger(R.integer.pref_text_size_default));
		int backgroundColor = sharedPref.getInt(Settings.KEY_PREF_SCREENSAVER_BACKGROUND_COLOR, this.getResources().getColor(R.color.pref_background_color_default));

		TextView quotationTextView = (TextView) findViewById(R.id.quotation);
		quotationTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textPrimarySize);

		findViewById(R.id.quotation_container).getLayoutParams().width = screenWidth*width/100;

		findViewById(R.id.belespritetbonnehumeur_screensaver).setBackgroundColor(backgroundColor);

		setScreenBright(sharedPref.getBoolean(Settings.KEY_PREF_SCREENSAVER_SCREEN_BRIGHT, this.getResources().getBoolean(R.bool.pref_screensaver_screen_bright_default)));

		updateQuotation();
	}

	@Override
	public void onDreamingStarted() {
		log.debug("onDreamingStarted");
		handler.removeCallbacks(updateRunner);
		scheduleUpdate();
	}

	@Override
	public void onDreamingStopped() {
		log.debug("onDreamingStopped");
		handler.removeCallbacks(updateRunner);
	}

	private void scheduleUpdate() {
		handler.postDelayed(updateRunner, (long) ((random.nextDouble() * 4 + 1) * 60000));
	}

	private void updateQuotation() {
		View container = findViewById(R.id.quotation_container);
		ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) container.getLayoutParams();
		layoutParams.horizontalBias = random.nextFloat();
		layoutParams.verticalBias = random.nextFloat();
		container.setLayoutParams(layoutParams);

		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		int textPrimaryColor = sharedPref.getInt(Settings.KEY_PREF_SCREENSAVER_TEXT_PRIMARY_COLOR, this.getResources().getColor(R.color.pref_text_primary_color_default));
		int textSecondaryColor = sharedPref.getInt(Settings.KEY_PREF_SCREENSAVER_TEXT_SECONDARY_COLOR, this.getResources().getColor(R.color.pref_text_secondary_color_default));
		Typeface typeface = FontUtils.getTypefaceFromBase64(sharedPref.getString(Settings.KEY_PREF_SCREENSAVER_TEXT_FONT, null));

		QuotationBean quotation = QuotationUtils.getQuotation(this);
		((TextView) findViewById(R.id.quotation)).setText(quotation.getSpannedText(this, textPrimaryColor, textSecondaryColor, typeface));
	}

}
