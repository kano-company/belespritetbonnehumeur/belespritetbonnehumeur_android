package company.kano.belespritetbonnehumeur.android.bean;

import company.kano.belespritetbonnehumeur.R;
import company.kano.belespritetbonnehumeur.android.util.FontUtils;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AlignmentSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class QuotationBean {

	private String quotation;
	private String author;
	private String work;
	private String url;

	public Spanned getSpannedText(Context context, Integer textPrimaryColor, Integer textSecondaryColor, Typeface typeface) {
		SpannableStringBuilder spannedText = new SpannableStringBuilder(context.getString(R.string.lbl_quotation, quotation));
		int start = 0;
		int end = spannedText.length();
		spannedText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		spannedText.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		if (textPrimaryColor != null) {
			spannedText.setSpan(new ForegroundColorSpan(textPrimaryColor), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		if (typeface != null) {
			spannedText.setSpan(FontUtils.getSpan(Typeface.create(typeface, Typeface.BOLD)), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		}

		if (author != null) {
			spannedText.append('\n').append(author);
			end = spannedText.length();
			start = end - author.length();
			spannedText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_OPPOSITE), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannedText.setSpan(new StyleSpan(Typeface.ITALIC), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannedText.setSpan(new RelativeSizeSpan(0.875f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			if (textSecondaryColor != null) {
				spannedText.setSpan(new ForegroundColorSpan(textSecondaryColor), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			}
			if (typeface != null) {
				spannedText.setSpan(FontUtils.getSpan(Typeface.create(typeface, Typeface.ITALIC)), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			}
		}

		if (work != null) {
			spannedText.append('\n').append(work);
			end = spannedText.length();
			start = end - work.length();
			spannedText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_OPPOSITE), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			spannedText.setSpan(new RelativeSizeSpan(0.875f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			if (textSecondaryColor != null) {
				spannedText.setSpan(new ForegroundColorSpan(textSecondaryColor), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			}
			if (typeface != null) {
				spannedText.setSpan(FontUtils.getSpan(typeface), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			}
		}

		return spannedText;
	}

}
