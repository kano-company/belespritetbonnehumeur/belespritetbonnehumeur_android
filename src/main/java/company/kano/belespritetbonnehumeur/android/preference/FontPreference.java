package company.kano.belespritetbonnehumeur.android.preference;

import company.kano.belespritetbonnehumeur.R;
import company.kano.belespritetbonnehumeur.android.util.FontUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultCaller;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FontPreference extends Preference {

	private final static String FONT_MIME_TYPE = "font/*";

	private ActivityResultLauncher<String> activityResultLauncher;

	public FontPreference(Context context, AttributeSet attrs) {
		super(context, attrs);

		setWidgetLayoutResource(R.layout.pref_font_widget);
	}

	public FontPreference(Context context) {
		this(context, null);
	}

	public void setValue(Uri fontUri) {
		if (fontUri != null) {
			try (InputStream fontInputStream = getContext().getContentResolver().openInputStream(fontUri)) {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				byte[] buf = new byte[4096];
				int len;
				while ((len = fontInputStream.read(buf)) != -1) {
					outputStream.write(buf, 0, len);
				}
				if (FontUtils.getTypefaceFromByteArray(outputStream.toByteArray()) != null) {
					persistString(Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT));
					notifyChanged();
				}
			} catch (IOException e) {
				log.error("Erreur lors de l'enregistrement de la police.", e);
			}
		} else {
			persistString(null);
			notifyChanged();
		}
	}

	@Override
	public CharSequence getSummary() {
		Typeface font = FontUtils.getTypefaceFromBase64(getPersistedString(null));
		if (font != null) {
			SpannableString spannableSummary = new SpannableString(super.getSummary());
			spannableSummary.setSpan(FontUtils.getSpan(font), 0, spannableSummary.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			return spannableSummary;
		}
		return super.getSummary();
	}

	@Override
	public void onBindViewHolder(PreferenceViewHolder holder) {
		super.onBindViewHolder(holder);

		Button resetBtn = (Button) holder.findViewById(R.id.btn_reset);
		resetBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				setValue(null);
				notifyChanged();
			}
		});
	}

	@Override
	public void onClick() {
		activityResultLauncher.launch(FONT_MIME_TYPE);
	}

	public void registerForActivityResult(ActivityResultCaller activityResultCaller) {
		activityResultLauncher = activityResultCaller.registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
			@Override
			public void onActivityResult(Uri result) {
				if (result != null) {
					setValue(result);
				}
			}
		});
	}

}
