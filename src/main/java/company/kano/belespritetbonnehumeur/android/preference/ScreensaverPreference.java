package company.kano.belespritetbonnehumeur.android.preference;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.util.AttributeSet;
import androidx.preference.Preference;

public class ScreensaverPreference extends Preference {

	public ScreensaverPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		setPersistent(false);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
			Intent intent = new Intent(Settings.ACTION_DREAM_SETTINGS);
			setEnabled(intent.resolveActivity(context.getPackageManager()) != null);
			setIntent(intent);
		} else {
			setEnabled(false);
		}
	}

	public ScreensaverPreference(Context context) {
		this(context, null);
	}

	@Override
	public void setPersistent (boolean persistent) {
		super.setPersistent(false);
	}

}
