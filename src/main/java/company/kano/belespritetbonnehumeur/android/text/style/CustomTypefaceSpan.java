package company.kano.belespritetbonnehumeur.android.text.style;

import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import androidx.annotation.NonNull;

public class CustomTypefaceSpan extends MetricAffectingSpan {

	private Typeface typeface;

	public CustomTypefaceSpan(Typeface typeface) {
		this.typeface = typeface;
	}

	@Override
	public void updateMeasureState(@NonNull TextPaint textPaint) {
		textPaint.setTypeface(typeface);
	}

	@Override
	public void updateDrawState(TextPaint textPaint) {
		textPaint.setTypeface(typeface);
	}
}
