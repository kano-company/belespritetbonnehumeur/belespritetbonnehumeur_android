package company.kano.belespritetbonnehumeur.android.util;

import company.kano.belespritetbonnehumeur.android.text.style.CustomTypefaceSpan;

import java.io.File;
import java.io.FileOutputStream;

import android.graphics.Typeface;
import android.os.Build;
import android.text.style.MetricAffectingSpan;
import android.text.style.TypefaceSpan;
import android.util.Base64;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FontUtils {

	public static Typeface getTypefaceFromBase64(String fontBase64) {
		if (fontBase64 != null) {
			return getTypefaceFromByteArray(Base64.decode(fontBase64, Base64.DEFAULT));
		}
		return null;
	}

	public static Typeface getTypefaceFromByteArray(byte[] fontByteArray) {
		if (fontByteArray != null) {
			try {
				File fontFile = File.createTempFile("font", null);
				try (FileOutputStream temp = new FileOutputStream(fontFile)) {
					temp.write(fontByteArray);
				}
				Typeface typeface = Typeface.createFromFile(fontFile);
				fontFile.delete();
				return typeface;
			} catch (Exception e) {
				log.error("Erreur lors de la création de la police.", e);
			}
		}
		return null;
	}

	public static MetricAffectingSpan getSpan(Typeface typeface) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
			return new TypefaceSpan(typeface);
		}
		return new CustomTypefaceSpan(typeface);
	}

}
