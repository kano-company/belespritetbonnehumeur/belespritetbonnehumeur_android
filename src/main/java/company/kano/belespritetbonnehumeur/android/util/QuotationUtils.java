package company.kano.belespritetbonnehumeur.android.util;

import company.kano.belespritetbonnehumeur.android.bean.QuotationBean;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Date;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class QuotationUtils {

	private static final String FILENAME = "citations.csv";
	private static final String NAME = "quotation";
	private static final String KEY_NEXT_TIME = "next_time";
	private static final String KEY_QUOTATION = "quotation";
	private static final String KEY_AUTHOR = "author";
	private static final String KEY_WORK = "work";
	private static final String KEY_URL = "url";

	public static QuotationBean getQuotation(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
		long nextTime = sharedPreferences.getLong(KEY_NEXT_TIME, Long.MIN_VALUE);
		long now = new Date().getTime();

		log.debug("now: {} / nextTime: {}", now, nextTime);

		if (nextTime < now) {
			Random random = new Random();

			try (Reader reader = new InputStreamReader(context.getAssets().open(FILENAME), "UTF-8")) {
				CSVParser parser = new CSVParser(
						reader,
						CSVFormat.DEFAULT.builder()
								.setRecordSeparator('\n')
								.setNullString("")
								.setHeader(KEY_AUTHOR, KEY_WORK, KEY_QUOTATION, KEY_URL)
								.build());
				List<CSVRecord> records = parser.getRecords();
				int index = Math.abs(random.nextInt()) % records.size();
				log.debug("index: {}", index);
				CSVRecord record = records.get(index);

				sharedPreferences.edit()
						.putLong(KEY_NEXT_TIME, now + (long) ((Math.sqrt(random.nextDouble())*18+6)*3600000))
						.putString(KEY_QUOTATION, record.get(KEY_QUOTATION))
						.putString(KEY_AUTHOR, record.get(KEY_AUTHOR))
						.putString(KEY_WORK, record.get(KEY_WORK))
						.putString(KEY_URL, record.get(KEY_URL))
						.apply();
			} catch (IOException e) {
				log.error("erreur lors de la récupération de la citation", e);
			}
		}

		return QuotationBean.builder()
				.quotation(sharedPreferences.getString(KEY_QUOTATION, null))
				.author(sharedPreferences.getString(KEY_AUTHOR, null))
				.work(sharedPreferences.getString(KEY_WORK, null))
				.url(sharedPreferences.getString(KEY_URL, null))
				.build();
	}

}
