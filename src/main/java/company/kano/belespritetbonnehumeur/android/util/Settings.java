package company.kano.belespritetbonnehumeur.android.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Settings {

	public static final String KEY_PREF_WIDGET_TEXT_SIZE = "pref_text_size";
	public static final String KEY_PREF_WIDGET_TEXT_PRIMARY_COLOR = "pref_text_primary_color";
	public static final String KEY_PREF_WIDGET_TEXT_SECONDARY_COLOR = "pref_text_secondary_color";
	public static final String KEY_PREF_WIDGET_BACKGROUND_COLOR = "pref_background_color";
	public static final String KEY_PREF_SCREENSAVER_TEXT_FONT = "pref_screensaver_text_font";
	public static final String KEY_PREF_SCREENSAVER_TEXT_WIDTH = "pref_screensaver_text_width";
	public static final String KEY_PREF_SCREENSAVER_TEXT_SIZE = "pref_screensaver_text_size";
	public static final String KEY_PREF_SCREENSAVER_TEXT_PRIMARY_COLOR = "pref_screensaver_text_primary_color";
	public static final String KEY_PREF_SCREENSAVER_TEXT_SECONDARY_COLOR = "pref_screensaver_text_secondary_color";
	public static final String KEY_PREF_SCREENSAVER_BACKGROUND_COLOR = "pref_screensaver_background_color";
	public static final String KEY_PREF_SCREENSAVER_SCREEN_BRIGHT = "pref_screensaver_screen_bright";

}
