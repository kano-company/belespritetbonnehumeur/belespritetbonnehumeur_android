# Bel Esprit & Bonne Humeur

![Bel Esprit & Bonne Humeur - (Re)Découvrez de belles citations, partagez du positif, partagez du bonheur. - « Ce qui compte ne peut pas toujours être compté, et ce qui peut être compté ne compte pas forcément. » Albert Einstein - « Plutôt que de maudire les ténèbres, allumons une chandelle, si petite soit-elle. » Confucius - « Cherche un arbre et laisse lui t'apprendre le calme. » Eckhart Tolle](src/main/svg/featureGraphic.svg)

Toi aussi tu vois le verre à moitié plein ? Il y a ce que tu perçois, et il y a tout ce que ton esprit ajoute par-dessus !  
Si toi aussi tu penses que "soit je réussis, soit j'apprends, dans tous les cas je m'en retrouve grandi, je m'enrichis", alors bienvenue. Et si tu penses autrement, alors bienvenue aussi. Parce que la différence, c'est ouvrir d'autres perspectives, c'est élargir le champ des possibles.

Je ne sais pas ce que deviendra cette application. Je ne peux que poser l'intention de partager ici de belles énergies. Un instant de lumière, un moment de bonheur, une journée pleine de joie, une vie remplie d'amour.

Ah oui, au fait, elle fait quoi cette application ? Elle affiche simplement une citation sur votre téléphone ou votre tablette sous la forme d'un widget ou d'un écran de veille (Paramètres > Écran > Économiseur d'écran). Vous ne choisissez ni la citation, ni la durée pendant laquelle elle vous accompagnera avant d'être remplacée par une autre. Mais vous pouvez personnaliser les couleurs et tailles.

Pour garder le sourire, pour rester zen, pour philosopher ou encore pour méditer. D'Albert Einstein à Yoda en passant par Antoine de Saint-Exupéry, Bouddha, Confucius, Épicure, Gandhi, Léonard de Vinci, Nelson Mandela, Paulo Coelho, Socrate, Victor Hugo et bien d'autres...

[L'application android](https://play.google.com/store/apps/details?id=company.kano.belespritetbonnehumeur) est disponible sur le play store.

## Licence

Bel Esprit & Bonne Humeur est distribué sous licence [Apache v2.0](https://www.apache.org/licenses/LICENSE-2.0).
