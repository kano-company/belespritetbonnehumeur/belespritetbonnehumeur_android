pipeline {
	agent any
	options {
		buildDiscarder(logRotator(numToKeepStr: '25', artifactNumToKeepStr: '50'))
	}
	tools {
		gradle 'gradle-7.4.1'
	}
	environment {
		GRADLE_OPTS='-Dorg.gradle.daemon=false'
	}
	stages {
		stage('build') {
			when {
				not { tag '' }
			}
			steps {
				withCredentials([
					usernamePassword(credentialsId: 'cicd.agent', usernameVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoUser', passwordVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoPassword'),
					file(credentialsId: 'kano.jks', variable: 'ORG_GRADLE_PROJECT_kanoStoreFile'),
					usernamePassword(credentialsId: 'kanoStore', usernameVariable: '', passwordVariable: 'ORG_GRADLE_PROJECT_kanoStorePassword'),
					usernamePassword(credentialsId: 'kano2Key', usernameVariable: 'ORG_GRADLE_PROJECT_kanoKeyAlias', passwordVariable: 'ORG_GRADLE_PROJECT_kanoKeyPassword')
				]) {
					sh 'gradle clean assemble'
				}
			}
		}
		stage('quality') {
			when {
				not { tag '' }
			}
			steps {
				withCredentials([
					usernamePassword(credentialsId: 'cicd.agent', usernameVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoUser', passwordVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoPassword'),
					file(credentialsId: 'kano.jks', variable: 'ORG_GRADLE_PROJECT_kanoStoreFile'),
					usernamePassword(credentialsId: 'kanoStore', usernameVariable: '', passwordVariable: 'ORG_GRADLE_PROJECT_kanoStorePassword'),
					usernamePassword(credentialsId: 'kano2Key', usernameVariable: 'ORG_GRADLE_PROJECT_kanoKeyAlias', passwordVariable: 'ORG_GRADLE_PROJECT_kanoKeyPassword')
				]) {
					sh 'gradle lint'
				}
				archiveArtifacts 'build/reports/lint*'
			}
		}
		stage('snapshot') {
			when {
				branch 'master'
				not { tag '' }
			}
			steps {
				withCredentials([
					usernamePassword(credentialsId: 'cicd.agent', usernameVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoUser', passwordVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoPassword'),
					file(credentialsId: 'kano.jks', variable: 'ORG_GRADLE_PROJECT_kanoStoreFile'),
					usernamePassword(credentialsId: 'kanoStore', usernameVariable: '', passwordVariable: 'ORG_GRADLE_PROJECT_kanoStorePassword'),
					usernamePassword(credentialsId: 'kano2Key', usernameVariable: 'ORG_GRADLE_PROJECT_kanoKeyAlias', passwordVariable: 'ORG_GRADLE_PROJECT_kanoKeyPassword')
				]) {
					sh 'gradle publish'
				}
			}
		}
		stage('release') {
			when {
				beforeInput true
				branch 'master'
				not { tag '' }
			}
			input {
				message 'Release?'
			}
			steps {
				sh 'git config user.name cicd.agent'
				sh 'git config user.email cicd.agent'
				sh 'git config --replace-all credential.helper \'!credentialHelper() { echo username=${GIT_USERNAME}; echo password=${GIT_PASSWORD}; }; credentialHelper\''
				withCredentials([
					usernamePassword(credentialsId: 'cicd.agent', usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD'),
					usernamePassword(credentialsId: 'cicd.agent', usernameVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoUser', passwordVariable: 'ORG_GRADLE_PROJECT_kanoMvnRepoPassword'),
					file(credentialsId: 'kano.jks', variable: 'ORG_GRADLE_PROJECT_kanoStoreFile'),
					usernamePassword(credentialsId: 'kanoStore', usernameVariable: '', passwordVariable: 'ORG_GRADLE_PROJECT_kanoStorePassword'),
					usernamePassword(credentialsId: 'kano2Key', usernameVariable: 'ORG_GRADLE_PROJECT_kanoKeyAlias', passwordVariable: 'ORG_GRADLE_PROJECT_kanoKeyPassword')
				]) {
					sh 'gradle publishAndTag -Prelease'
				}
			}
			post {
				always {
					sh 'git config --unset credential.helper'
				}
			}
		}
		stage('apk publish') {
			when {
				beforeInput true
				branch 'master'
				not { tag '' }
			}
			input {
				message 'apk publish'
				parameters {
					choice choices: ['internal', 'alpha', 'beta'], description: 'Canal', name: 'channel'
					validatingString defaultValue: '0', description: 'Pourcentage de déploiement', failedValidationMessage: '', name: 'rolloutPercentage', regex: '^(100|[1-9]?[0-9])$'
				}
			}
			steps {
				androidApkUpload googleCredentialsId: 'kanoGoogleCredentialsId',
					filesPattern: 'build/outputs/bundle/release/belespritetbonnehumeur_android-release.aab',
					releaseName: '{versionName}',
					trackName: channel,
					rolloutPercentage: rolloutPercentage,
					recentChangeList: [
						[language: 'fr-FR', text: readFile(encoding: 'UTF-8', file:'src/main/store/fr-FR/whatsnew.txt')]
					]
			}
		}
	}
}
